<!ENTITY generalTabLabel "General">
<!ENTITY CardBookTabLabel "Pestaña">
<!ENTITY editionTabLabel "Edición">
<!ENTITY IMPPsTabLabel "Llamadas">
<!ENTITY customFieldsTabLabel "Campos personalizados">
<!ENTITY listsTabLabel "Listas">
<!ENTITY syncTabLabel "Sincronizaciones">
<!ENTITY emailTabLabel "Correo electrónico">
<!ENTITY birthdaylistTabLabel "Cumpleaños">
<!ENTITY synclistTabLabel "Recordatorios de los cumpleaños">

<!ENTITY integrationGroupboxLabel "Integración con Thunderbird">
<!ENTITY autocompletionLabel "Autocompletado de direcciones de correo electrónico">
<!ENTITY autocompletionAccesskey "e">
<!ENTITY autocompletionTooltip "Extiende el autocompletado estándar de contactos en las libretas de direcciones de Thunderbird con un autocompletado de contactos en las libretas de direcciones de CardBook.">
<!ENTITY autocompleteSortByPopularityLabel "Ordenar las direcciones de correo electrónico por popularidad">
<!ENTITY autocompleteSortByPopularityAccesskey "T">
<!ENTITY autocompleteProposeConcatEmailsLabel "Proponer las direcciones de correo electrónico concatenadas por contacto">
<!ENTITY autocompleteProposeConcatEmailsAccesskey "c">
<!ENTITY autocompleteShowAddressbookLabel "Mostrar el nombre de la libreta de direcciones">
<!ENTITY autocompleteShowAddressbookAccesskey "M">
<!ENTITY autocompleteWithColorLabel "Usar el color de la libreta de direcciones">
<!ENTITY autocompleteWithColorAccesskey "c">
<!ENTITY autocompleteRestrictSearchLabel "Restringir los campos en los cuales buscar">
<!ENTITY autocompleteRestrictSearchAccesskey "R">
<!ENTITY chooseAutocompleteRestrictSearchFieldsButtonLabel "Modificar">
<!ENTITY chooseAutocompleteRestrictSearchFieldsButtonAccesskey "M">
<!ENTITY resetAutocompleteRestrictSearchFieldsButtonLabel "Vaciar">
<!ENTITY resetAutocompleteRestrictSearchFieldsButtonAccesskey "R">
<!ENTITY useColorLabel "Usar el color de la libreta de direcciones para:">
<!ENTITY useColorBackgroundLabel "El color del fondo">
<!ENTITY useColorBackgroundAccesskey "f">
<!ENTITY useColorTextLabel "El color del texto">
<!ENTITY useColorTextAccesskey "x">
<!ENTITY useColorNothingLabel "No usar">
<!ENTITY useColorNothingAccesskey "N">
<!ENTITY exclusiveLabel "Fuente única de contactos">
<!ENTITY exclusiveAccesskey "U">
<!ENTITY exclusiveTooltip "Si deshabilita esta opción, podrá recuperar las estrellas amarillas, el nombre a mostrar del correo electrónico y el autocompletado del correo electrónico en los contactos de las libretas de direcciones estándar de Thunderbird.">
<!ENTITY accountsRestrictionsLabel "Restricciones de acceso para las cuentas de correo electrónico">
<!ENTITY accountsRestrictionsEnabledLabel "Habilitado">
<!ENTITY accountsRestrictionsMailNameLabel "Cuenta de correo electrónico">
<!ENTITY accountsRestrictionsABNameLabel "Libreta de direcciones">
<!ENTITY accountsRestrictionsCatNameLabel "Categoría">
<!ENTITY accountsRestrictionsIncludeNameLabel "Tipo">
<!ENTITY addRestrictionLabel "Añadir">
<!ENTITY addRestrictionAccesskey "A">
<!ENTITY renameRestrictionLabel "Editar">
<!ENTITY renameRestrictionAccesskey "E">
<!ENTITY deleteRestrictionLabel "Eliminar">
<!ENTITY deleteRestrictionAccesskey "S">
<!ENTITY emailsCollectionLabel "Recopilar las direcciones de los correos electrónicos de salida">
<!ENTITY emailsCollectionTooltip "Extiende la recopilación de direcciones de correo electrónico salientes estándar de Thunderbird. Si está habilitado, las direcciones de correo electrónico que no estén presentes en ninguna libreta de direcciones de CardBook se añadirán a las libretas de direcciones seleccionadas.">
<!ENTITY emailsCollectionEnabledLabel "Habilitado">
<!ENTITY emailsCollectionMailNameLabel "Cuenta de correo electrónico">
<!ENTITY emailsCollectionABNameLabel "Libreta de direcciones">
<!ENTITY emailsCollectionCatNameLabel "Categoría">
<!ENTITY addEmailsCollectionLabel "Añadir">
<!ENTITY addEmailsCollectionAccesskey "A">
<!ENTITY renameEmailsCollectionLabel "Editar">
<!ENTITY renameEmailsCollectionAccesskey "E">
<!ENTITY deleteEmailsCollectionLabel "Eliminar">
<!ENTITY deleteEmailsCollectionAccesskey "S">
<!ENTITY logGroupboxLabel "Registro">
<!ENTITY debugModeLabel "Modo de depuración">
<!ENTITY debugModeAccesskey "d">
<!ENTITY debugModeTooltip "Habilite el modo de depuración para ampliar el registro.">
<!ENTITY statusInformationLineNumberLabel "Número de líneas guardadas en el archivo de registro:">
<!ENTITY statusInformationLineNumberAccesskey "N">

<!ENTITY tabsEditingGroupboxLabel "Mostrar las pestañas">
<!ENTITY noteTabLabel "Notas">
<!ENTITY noteTabAccesskey "e">
<!ENTITY listTabLabel "Lista">
<!ENTITY listTabAccesskey "L">
<!ENTITY mailPopularityTabLabel "Popularidad de los correos electrónicos">
<!ENTITY mailPopularityTabAccesskey "u">
<!ENTITY technicalTabLabel "Técnica">
<!ENTITY technicalTabAccesskey "T">
<!ENTITY vCardTabLabel "vCard">
<!ENTITY vCardTabAccesskey "V">
<!ENTITY advancedTab1Label "Avanzado">
<!ENTITY localizeEditingGroupboxLabel "Ubicar con">
<!ENTITY localizeEngineOpenStreetMapLabel "OpenStreetMap">
<!ENTITY localizeEngineOpenStreetMapAccesskey "S">
<!ENTITY localizeEngineGoogleMapsLabel "Google Maps">
<!ENTITY localizeEngineGoogleMapsAccesskey "G">
<!ENTITY localizeEngineBingMapsLabel "Bing Maps">
<!ENTITY localizeEngineBingMapsAccesskey "B">
<!ENTITY localizeTargetLabel "Abrir los vínculos externos">
<!ENTITY localizeTargetInLabel "En Thunderbird">
<!ENTITY localizeTargetInAccesskey "D">
<!ENTITY localizeTargetOutLabel "Fuera de Thunderbird">
<!ENTITY localizeTargetOutAccesskey "H">
<!ENTITY showNameAsLabel "Mostrar el nombre como">
<!ENTITY showNameAsLFLabel "Apellidos Nombre">
<!ENTITY showNameAsLFAccesskey "A">
<!ENTITY showNameAsLFCommaLabel "Apellidos, Nombre">
<!ENTITY showNameAsLFCommaAccesskey "i">
<!ENTITY showNameAsFLLabel "Nombre Apellidos">
<!ENTITY showNameAsFLAccesskey "o">
<!ENTITY showNameAsDSPLabel "Nombre mostrado">
<!ENTITY showNameAsDSPAccesskey "m">
<!ENTITY adrFormulaCaption "Dirección">
<!ENTITY adrFormulaLabel "Formato de las direcciones:">
<!ENTITY adrFormulaAccesskey "F">
<!ENTITY resetAdrFormulaLabel "Restaurar valores predeterminados">
<!ENTITY resetAdrFormulaAccesskey "R">
<!ENTITY miscEditingGroupboxLabel "Diverso">
<!ENTITY preferEmailEditionLabel "En vez de enviar un correo electrónico, hacer un doble clic sobre un contacto activa a la edición">
<!ENTITY preferEmailEditionAccesskey "A">
<!ENTITY dateDisplayedFormatLabel "Formato de fecha:">
<!ENTITY dateDisplayedFormatAccesskey "d">
<!ENTITY defaultRegionLabel "Región por defecto:">
<!ENTITY defaultRegionAccesskey "e">
<!ENTITY defaultRegionTooltip "Esta región será la región por defecto para las direcciones y será usada para formatear los números de teléfono.">
<!ENTITY preferenceGroupboxLabel "Campo PREF">
<!ENTITY usePreferenceValueLabel "Usar un valor para las preferencias (solo para vCard 4.0)">
<!ENTITY usePreferenceValueAccesskey "v">
<!ENTITY preferenceValueLabel "Etiqueta del valor del campo PREF">
<!ENTITY preferenceValueAccesskey "L">

<!ENTITY editionGroupboxLabel "Selector de campos">
<!ENTITY editionWarn "Al editar un contacto, CardBook solo mostrará los campos seleccionados.">
<!ENTITY selectAllFieldsLabel "Seleccionar todo">

<!ENTITY ABtypesGroupboxLabel "Libreta de direcciones">
<!ENTITY typesCategoryGoogleLabel "Google">
<!ENTITY typesCategoryGoogleAccesskey "G">
<!ENTITY typesCategoryAppleLabel "Apple">
<!ENTITY typesCategoryAppleAccesskey "p">
<!ENTITY typesCategoryYahooLabel "Yahoo!">
<!ENTITY typesCategoryYahooAccesskey "Y">
<!ENTITY typesCategoryOthersLabel "Otros">
<!ENTITY typesCategoryOthersAccesskey "O">
<!ENTITY typesGroupboxLabel "Tipos">
<!ENTITY typesCategoryAdrLabel "Dirección">
<!ENTITY typesCategoryAdrAccesskey "D">
<!ENTITY typesCategoryEmailLabel "Correo electrónico">
<!ENTITY typesCategoryEmailAccesskey "C">
<!ENTITY typesCategoryImppLabel "IMPP">
<!ENTITY typesCategoryImppAccesskey "I">
<!ENTITY typesCategoryTelLabel "Teléfono">
<!ENTITY typesCategoryTelAccesskey "T">
<!ENTITY typesCategoryUrlLabel "URL">
<!ENTITY typesCategoryUrlAccesskey "U">
<!ENTITY typesLabelLabel "Etiqueta">
<!ENTITY addTypeLabel "Añadir">
<!ENTITY addTypeAccesskey "A">
<!ENTITY renameTypeLabel "Editar">
<!ENTITY renameTypeAccesskey "E">
<!ENTITY deleteTypeLabel "Eliminar">
<!ENTITY deleteTypeAccesskey "S">
<!ENTITY resetTypeLabel "Vaciar">
<!ENTITY resetTypeAccesskey "R">

<!ENTITY IMPPGroupboxWarn "Esta configuración permite llamadar por Internet utilizando el protocolo deseado.">
<!ENTITY IMPPGroupboxLabel "Llamadas por Internet">
<!ENTITY preferIMPPPrefLabel "Preferir las llamadas de Internet marcadas con">
<!ENTITY preferIMPPPrefAccesskey "P">
<!ENTITY preferIMPPPrefWarn "Si no se marca ninguna dirección, se propone una conexión a todas las direcciones del contacto.">
<!ENTITY IMPPCodeLabel "Código">
<!ENTITY IMPPLabelLabel "Etiqueta">
<!ENTITY IMPPProtocolLabel "Protocolo">
<!ENTITY addIMPPLabel "Añadir">
<!ENTITY addIMPPAccesskey "A">
<!ENTITY renameIMPPLabel "Editar">
<!ENTITY renameIMPPAccesskey "E">
<!ENTITY deleteIMPPLabel "Borrar">
<!ENTITY deleteIMPPAccesskey "B">
<!ENTITY URLPhoneGroupboxLabel "Llamadas telefónicas por URL">
<!ENTITY URLPhoneGroupboxWarn "Esta sección se aplica solamente si el protocolo URL para teléfonos está seleccionado. La URL tiene que incluir $1, refiriéndose al número de teléfono.">
<!ENTITY URLPhoneURLLabel "URL:">
<!ENTITY URLPhoneURLAccesskey "U">
<!ENTITY URLPhoneUserLabel "Usuario:">
<!ENTITY URLPhoneUserAccesskey "s">
<!ENTITY URLPhonePasswordLabel "Contraseña:">
<!ENTITY URLPhonePasswordAccesskey "C">
<!ENTITY URLPhonePasswordShowLabel "Ver">
<!ENTITY URLPhonePasswordShowAccesskey "V">
<!ENTITY URLPhoneBackgroundLabel "Ejecutar en segundo plano">
<!ENTITY URLPhoneBackgroundAccesskey "S">
<!ENTITY encryptionGroupboxLabel "Cifrado">
<!ENTITY encryptionExplanation "Cardbook puede cifrar sus datos locales. Para hacerlo, usará una clave secreta guardada en el Administrador de Contraseñas. Si pierde esta clave, será imposible recuperar sus datos locales. Para una protección completa, se recomienda activar la funcionalidad de contraseña maestra de Thunderbird.">
<!ENTITY localDataEncryptionEnabledLabel "Cifrar las tarjetas almacenadas localmente">
<!ENTITY localDataEncryptionEnabledAccesskey "i">

<!ENTITY customFieldRankLabel "Rango">
<!ENTITY customFieldCodeLabel "Nombre del campo">
<!ENTITY customFieldLabelLabel "Etiqueta del campo">
<!ENTITY customFieldsPersTitleLabel "Información personal">
<!ENTITY customFieldsPersTitleAccesskey "P">
<!ENTITY customFieldsOrgTitleLabel "Información profesional">
<!ENTITY customFieldsOrgTitleAccesskey "r">
<!ENTITY addCustomFieldsLabel "Añadir">
<!ENTITY addCustomFieldsAccesskey "A">
<!ENTITY renameCustomFieldsLabel "Editar">
<!ENTITY renameCustomFieldsAccesskey "E">
<!ENTITY deleteCustomFieldsLabel "Eliminar">
<!ENTITY deleteCustomFieldsAccesskey "S">

<!ENTITY orgWarn "El campo ORG puede contener un valor estructurado, compuesto de elementos separados por el carácter PUNTO Y COMA (por ejemplo: compañia;servicio;departamento). Los campos siguientes describen las etiquetas de estos elementos.">
<!ENTITY orgRankLabel "Rango">
<!ENTITY orgLabelLabel "Etiqueta del campo">

<!ENTITY listPanelLabel "Si su servidor no admite categorías (como Google, Apple, Zimbra, etc…), las listas son una buena manera de enviar correos electrónicos a grupos. vCard 3.0 no admite listas de forma nativa, pero esto se puede lograr con campos personalizados, por lo que si desea utilizar listas en vCard 3.0, debe definir estos campos.">
<!ENTITY listGroupboxLabel "Campos personalizados para las listas">
<!ENTITY kindCustomLabel "Tipo:">
<!ENTITY kindCustomAccesskey "T">
<!ENTITY memberCustomLabel "Miembro:">
<!ENTITY memberCustomAccesskey "M">
<!ENTITY resetListLabel "Restaurar valores predeterminados">
<!ENTITY resetListAccesskey "R">

<!ENTITY syncGroupboxLabel "Sincronizaciones">
<!ENTITY syncWarn "Sincronizar es el único método para guardar sus modificaciones en un servidor remoto.">
<!ENTITY syncAfterChangeLabel "Sincronizar después de la edición">
<!ENTITY syncAfterChangeAccesskey "c">
<!ENTITY initialSyncLabel "Sincronizar al inicio de Thunderbird">
<!ENTITY initialSyncAccesskey "S">
<!ENTITY initialSyncDelayLabel "Retraso de sincronización al inicio (segundos):">
<!ENTITY initialSyncDelayAccesskey "R">
<!ENTITY solveConflictsLabel "Si un contacto se ha modificado en el servidor y localmente:">
<!ENTITY solveConflictsLocalLabel "Preferir la modificación local">
<!ENTITY solveConflictsLocalAccesskey "L">
<!ENTITY solveConflictsRemoteLabel "Preferir la modificación del servidor">
<!ENTITY solveConflictsRemoteAccesskey "S">
<!ENTITY solveConflictsUserLabel "Preguntar al usuario">
<!ENTITY solveConflictsUserAccesskey "U">
<!ENTITY maxModifsPushedLabel "Número máximo de modificaciones enviadas en cada sincronización:">
<!ENTITY maxModifsPushedAccesskey "m">
<!ENTITY requestsGroupboxLabel "Consultas">
<!ENTITY requestsTimeoutLabel "Tiempo máximo (segundos):">
<!ENTITY requestsTimeoutAccesskey "D">
<!ENTITY multigetLabel "Para las consultas GET, agrupar los contactos por:">
<!ENTITY multigetAccesskey "G">
<!ENTITY discoveryGroupboxLabel "Descubrimiento de libretas de direcciones">
<!ENTITY discoveryWarn "En cada sincronización, se ejecutará una verificación en las URL siguientes para buscar libretas de direcciones nuevas o eliminadas.">
<!ENTITY discoveryAccountsGroupboxLabel "Cuentas">
<!ENTITY advancedSyncGroupboxLabel "Propiedades avanzadas">
<!ENTITY decodeReportLabel "Descodificar las URL para las consultas REPORT">
<!ENTITY decodeReportAccesskey "D">

<!ENTITY miscEmailGroupboxLabel "Envío de correos electrónicos">
<!ENTITY preferEmailPrefLabel "Preferir direcciones de correo electrónico marcadas con ">
<!ENTITY preferEmailPrefAccesskey "P">
<!ENTITY preferEmailPrefWarn "Si no se marca ninguna dirección, el mensaje se envía a todas las direcciones del contacto.">
<!ENTITY warnEmptyEmailsLabel "Avisarme cuando trato de enviar un correo electrónico a un contacto que no tiene dirección de correo">
<!ENTITY warnEmptyEmailsAccesskey "M">
<!ENTITY useOnlyEmailLabel "Usar solo direcciones de correo electrónico, sin mostrar sus nombres">
<!ENTITY useOnlyEmailAccesskey "U">
<!ENTITY attachVCardWarn "Para asociar automáticamente una vCard a sus correos electrónicos, seleccione una vCard para cada cuenta de correo. Si está activada, CardBook adjuntará esta vCard a cada correo electrónico enviado desde esta cuenta.">
<!ENTITY attachVCardGroupboxLabel "Asociar una vCard">
<!ENTITY accountsVCardsEnabledLabel "Habilitado">
<!ENTITY accountsVCardsMailLabel "Cuenta de correo">
<!ENTITY accountsVCardsABNameLabel "Libreta de direcciones">
<!ENTITY accountsVCardsFileNameLabel "Archivo adjunto">
<!ENTITY accountsVCardsFnLabel "Contacto">
<!ENTITY addVCardLabel "Añadir">
<!ENTITY addVCardAccesskey "A">
<!ENTITY renameVCardLabel "Modificar">
<!ENTITY renameVCardAccesskey "M">
<!ENTITY deleteVCardLabel "Eliminar">
<!ENTITY deleteVCardAccesskey "S">

<!ENTITY birthdayListWarn "Los cumpleaños solo se buscan en las libretas de direcciones de CardBook.">
<!ENTITY addressbooksGroupboxLabel "Libretas de direcciones">
<!ENTITY selectAllABsLabel "Seleccionar todo">
<!ENTITY searchForLabel "Buscar:">
<!ENTITY bdayLabel "Fecha de nacimiento">
<!ENTITY bdayAccesskey "n">
<!ENTITY anniversaryLabel "Cumpleaños (solamente para vCard 4.0)">
<!ENTITY anniversaryAccesskey "C">
<!ENTITY deathdateLabel "Fecha de deceso (solamente para vCard 4.0)">
<!ENTITY deathdateAccesskey "d">
<!ENTITY eventsLabel "Eventos">
<!ENTITY eventsAccesskey "E">
<!ENTITY calendarsGroupboxLabel "Calendarios">
<!ENTITY selectAllCalendarsLabel "Seleccionar todo">

<!ENTITY remindersTabLabel "Recodatorios">
<!ENTITY remindViaPopupWindowGroupboxLabel "Recordatorio por ventana emergente">
<!ENTITY numberOfDaysForSearchingLabel "Número de días para buscar la existencia de cumpleaños:">
<!ENTITY numberOfDaysForSearchingAccesskey "N">
<!ENTITY showPopupOnStartupLabel "Ver una ventana emergente al iniciar">
<!ENTITY showPopupOnStartupAccesskey "V">
<!ENTITY showPeriodicPopupLabel "Ver una ventana emergente por día">
<!ENTITY showPeriodicPopupAccesskey "q">
<!ENTITY periodicPopupTimeLabel "Hora de visualización (hh:mm):">
<!ENTITY periodicPopupTimeAccesskey "H">
<!ENTITY showPopupEvenIfNoBirthdayLabel "Ver una ventana emergente cuando la lista de cumpleaños está vacía">
<!ENTITY showPopupEvenIfNoBirthdayAccesskey "P">
<!ENTITY remindViaLightningGroupboxLabel "Recordatorio por la extensión de calendario Lightning de Thunderbird">
<!ENTITY numberOfDaysForWritingLabel "Número de días para escribir los cumpleaños:">
<!ENTITY numberOfDaysForWritingAccesskey "o">
<!ENTITY syncWithLightningOnStartupLabel "Sincronizar con Lightning al inicio">
<!ENTITY syncWithLightningOnStartupAccesskey "S">
<!ENTITY calendarEntryTitleLabel "Título para los eventos en el calendario:">
<!ENTITY calendarEntryTitleTooltip "Modelo de título para los eventos del calendario. Este campo debe contener dos &#37;S, remplazados respectivamente por el nombre y la edad. Si el campo está vacío, entonces se utiliza un mensaje estándar.">
<!ENTITY calendarEntryTitleAccesskey "T">
<!ENTITY resetCalendarEntryTitleButtonLabel "Vaciar">
<!ENTITY resetCalendarEntryTitleButtonAccesskey "R">
<!ENTITY calendarEntryTimeLabel "Hora para los eventos del calendario (hh:mm):">
<!ENTITY calendarEntryTimeAccesskey "e">
<!ENTITY calendarEntryWholeDayLabel "Eventos del calendario de día entero">
<!ENTITY calendarEntryWholeDayAccesskey "j">
<!ENTITY calendarEntryAlarmLabel "Alarma(s) a n horas para los eventos del calendario (Ejemplo: 4,+1):">
<!ENTITY calendarEntryAlarmTooltip "Introduzca con cuántas horas antes del cumpleaños quiere recibir una alarma de Lightning. Si el campo está vacío, no habrá alarma. Si desea varias, introduzca las alarmas en una lista separada por comas.">
<!ENTITY calendarEntryAlarmAccesskey "n">
<!ENTITY calendarEntryCategoriesLabel "Categoría(s) para los eventos del calendario:">
<!ENTITY calendarEntryCategoriesTooltip "Una o varias categorías separadas por comas. Si el campo está vacío, entonces no se colocará ninguna categoría.">
<!ENTITY calendarEntryCategoriesAccesskey "g">
