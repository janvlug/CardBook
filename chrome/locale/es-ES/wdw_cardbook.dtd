<!ENTITY wdw_cardbookWindowLabel "CardBook">

<!ENTITY enableAccountsTooltip "Habilite o deshabilite la búsqueda y sincronización de libretas de direcciones.">
<!ENTITY colorAccountsTooltip "Resultados de búsqueda de las libretas de direcciones en color (se utilizan cuando las búsquedas abarcan varias libretas de direcciones)">
<!ENTITY readonlyAccountsTooltip "Configurar las libretas de direcciones en modo de solo lectura o lectura y escritura.">
<!ENTITY searchRemoteLabel "Esta libreta de direcciones muestra contactos solo tras una búsqueda">

<!ENTITY cardbookAccountMenuLabel "Libreta de direcciones">

<!ENTITY cardbookAccountMenuAddServerLabel "Nueva libreta de direcciones">
<!ENTITY cardbookAccountMenuEditServerLabel "Editar la libreta de direcciones">
<!ENTITY cardbookAccountMenuCloseServerLabel "Eliminar la libreta de direcciones">
<!ENTITY cardbookAccountMenuSyncLabel "Sincronizar la libreta de direcciones">
<!ENTITY cardbookAccountMenuSyncsLabel "Sincronizar todas las libreta de direcciones">

<!ENTITY cardbookContactsMenuLabel "Contactos">

<!ENTITY cardbookToolsMenuLabel "Herramientas">
<!ENTITY cardbookToolsMenuBirthdayListLabel "Ver la lista de cumpleaños">
<!ENTITY cardbookToolsMenuSyncLightningLabel "Añadir los cumpleaños al calendario">
<!ENTITY cardbookToolsMenuFindAllDuplicatesLabel "Buscar contactos duplicados en todas las libretas de direcciones">
<!ENTITY cardbookToolsMenuFindSingleDuplicatesLabel "Buscar contactos duplicados en la libreta de direcciones actual">
<!ENTITY cardbookToolsMenuPrefsLabel "Preferencias de CardBook">

<!ENTITY cardbookToolbarLabel "Barra de herramientas de CardBook">
<!ENTITY cardbookToolbarAccesskey "C">
<!ENTITY cardbookABPaneToolbarLabel "Barra de herramientas del panel de la libreta de direcciones de CardBook">
<!ENTITY cardbookABPaneToolbarAccesskey "p">
<!ENTITY CustomizeCardBookToolbarLabel "Personalizar…">
<!ENTITY CustomizeCardBookToolbarAccesskey "P">
<!ENTITY cardbookToolbarAddServerButtonLabel "Nueva libreta de direcciones">
<!ENTITY cardbookToolbarAddServerButtonTooltip "Añadir una libreta de direcciones local o remota">
<!ENTITY cardbookToolbarSyncButtonLabel "Sincronizar">
<!ENTITY cardbookToolbarSyncButtonTooltip "Sincronizar todas las libretas de direcciones remotas">
<!ENTITY cardbookToolbarWriteButtonLabel "Escribir">
<!ENTITY cardbookToolbarWriteButtonTooltip "Crear un nuevo mensaje">
<!ENTITY cardbookToolbarChatButtonLabel "Conectar a">
<!ENTITY cardbookToolbarChatButtonTooltip "Enviar un mensaje instantáneo o iniciar un chat">
<!ENTITY cardbookToolbarConfigurationButtonLabel "Preferencias">
<!ENTITY cardbookToolbarConfigurationButtonTooltip "Preferencias de CardBook">
<!ENTITY cardbookToolbarAddContactButtonLabel "Nuevo contacto">
<!ENTITY cardbookToolbarAddContactButtonTooltip "Crear un nuevo contacto">
<!ENTITY cardbookToolbarAddListButtonLabel "Nueva lista">
<!ENTITY cardbookToolbarAddListButtonTooltip "Nueva lista de correo">
<!ENTITY cardbookToolbarEditButtonLabel "Editar">
<!ENTITY cardbookToolbarEditButtonTooltip "Editar el contacto seleccionado">
<!ENTITY cardbookToolbarRemoveButtonLabel "Eliminar">
<!ENTITY cardbookToolbarRemoveButtonTooltip "Eliminar contacto seleccionado">
<!ENTITY cardbookToolbarPrintButtonLabel "Vista previa">
<!ENTITY cardbookToolbarPrintButtonTooltip "Vista previa de la selección">
<!ENTITY cardbookToolbarBackButtonLabel "Deshacer">
<!ENTITY cardbookToolbarBackButtonTooltip "Deshacer la última acción">
<!ENTITY cardbookToolbarForwardButtonLabel "Rehacer">
<!ENTITY cardbookToolbarForwardButtonTooltip "Rehacer una acción">
<!ENTITY cardbookToolbarAppMenuButtonLabel "Menú CardBook">
<!ENTITY cardbookToolbarAppMenuButtonTooltip "Menú CardBook">
<!ENTITY cardbookToolbarSearchBoxLabel "Búsqueda de contactos">
<!ENTITY cardbookToolbarSearchBoxTooltip "Búsqueda de contactos">
<!ENTITY cardbookToolbarComplexSearchLabel "Búsqueda guardada">
<!ENTITY cardbookToolbarComplexSearchTooltip "Crea una nueva carpeta de búsqueda guardada">
<!ENTITY cardbookToolbarThMenuButtonLabel "Menú de la aplicación">
<!ENTITY cardbookToolbarThMenuButtonTooltip "Mostrar el menú de &brandShortName;">
<!ENTITY cardbookToolbarABMenuLabel "Vista de libretas de direcciones">
<!ENTITY cardbookToolbarABMenuTooltip "Filtrar cómo se muestran las libretas de direcciones">

<!ENTITY generalTabLabel "General">
<!ENTITY mailPopularityTabLabel "Popularidad de los correos electrónicos">
<!ENTITY technicalTabLabel "Técnica">
<!ENTITY vCardTabLabel "vCard">

<!ENTITY categoriesGroupboxLabel "Categorías">
<!ENTITY noteTabLabel "Notas">

<!ENTITY miscGroupboxLabel "Diverso">
<!ENTITY othersGroupboxLabel "Otros">
<!ENTITY techGroupboxLabel "Técnica">
<!ENTITY labelGroupboxLabel "Etiquetas">
<!ENTITY class1Label "Clase">
<!ENTITY geoLabel "Geo">
<!ENTITY mailerLabel "Mailer">
<!ENTITY agentLabel "Agente">
<!ENTITY keyLabel "Clave">
<!ENTITY photolocalURILabel "Foto local">
<!ENTITY logolocalURILabel "Logo local">
<!ENTITY soundlocalURILabel "Sonido local">
<!ENTITY photoURILabel "Foto">
<!ENTITY logoURILabel "Logo">
<!ENTITY soundURILabel "Sonido">
<!ENTITY prodidLabel "Prodid">
<!ENTITY sortstringLabel "Cadena de clasificación (sort-string)">
<!ENTITY uidLabel "ID de contacto">
<!ENTITY versionLabel "Versión">
<!ENTITY tzLabel "Zona horaria">

<!ENTITY dirPrefIdLabel "ID de la libreta de direcciones">
<!ENTITY cardurlLabel "URL de la tarjeta">
<!ENTITY cacheuriLabel "URI de la caché">
<!ENTITY revLabel "Última actualización">
<!ENTITY etagLabel "Etag">

<!ENTITY localizeadrTreeLabel "Mostrar en el mapa">
<!ENTITY toEmailEmailTreeLabel "Escribir un nuevo mensaje">
<!ENTITY ccEmailEmailTreeLabel "Escribir un nuevo mensaje (copia/cc)">
<!ENTITY bccemailemailTreeLabel "Escribir un nuevo mensaje (copia oculta/bcc)">
<!ENTITY findemailemailTreeLabel "Buscar correos electrónicos relacionados con esta dirección de correo electrónico">
<!ENTITY findeventemailTreeLabel "Buscar eventos de calendario relacionados con esta dirección de correo electrónico">
<!ENTITY openURLTreeLabel "Abrir la URL">

<!ENTITY toEmailCardFromAccountsOrCatsLabel "Escribir un nuevo mensaje">
<!ENTITY toEmailCardFromCardsLabel "Escribir un nuevo mensaje">
<!ENTITY ccEmailCardFromAccountsOrCatsLabel "Escribir un nuevo mensaje (copia/cc)">
<!ENTITY ccEmailCardFromCardsLabel "Escribir un nuevo mensaje (copia/cc)">
<!ENTITY bccEmailCardFromAccountsOrCatsLabel "Escribir un nuevo mensaje (copia oculta/bcc)">
<!ENTITY bccEmailCardFromCardsLabel "Escribir un nuevo mensaje (copia oculta/bcc)">
<!ENTITY shareCardByEmailFromAccountsOrCatsLabel "Compartir por correo electrónico">
<!ENTITY shareCardByEmailFromCardsLabel "Compartir por correo electrónico">
<!ENTITY categoryLabel "Categoría">
<!ENTITY findEmailsFromCardsLabel "Buscar correos electrónicos relacionados con este contacto">
<!ENTITY findEventsFromCardsLabel "Buscar eventos de calendario relacionados con este contacto">
<!ENTITY localizeCardFromCardsLabel "Mostrar en el mapa">
<!ENTITY openURLCardFromCardsLabel "Abrir las URL">
<!ENTITY cutCardFromAccountsOrCatsLabel "Cortar">
<!ENTITY cutCardFromCardsLabel "Cortar">
<!ENTITY copyCardFromAccountsOrCatsLabel "Copiar">
<!ENTITY copyCardFromCardsLabel "Copiar">
<!ENTITY pasteCardFromAccountsOrCatsLabel "Pegar">
<!ENTITY pasteCardFromCardsLabel "Pegar">
<!ENTITY pasteEntryLabel "Pegar entrada">
<!ENTITY exportCardToFileLabel "Exportar a un archivo">
<!ENTITY exportCardToDirLabel "Exportar a un directorio">
<!ENTITY importCardFromFileLabel "Importar contactos desde un archivo">
<!ENTITY importCardFromDirLabel "Importar contactos desde un directorio">
<!ENTITY findDuplicatesFromAccountsOrCatsLabel "Buscar contactos duplicados en la libreta de direcciones actual">
<!ENTITY generateFnFromAccountsOrCatsLabel "Generar los nombres mostrados">
<!ENTITY mergeCardsFromCardsLabel "Fusionar los contactos">
<!ENTITY duplicateCardFromCardsLabel "Duplicar el contacto">
<!ENTITY convertListToCategoryFromCardsLabel "Convertir la lista en categoría">
<!ENTITY editAccountFromAccountsOrCatsLabel "Editar la libreta de direcciones">
<!ENTITY syncAccountFromAccountsOrCatsLabel "Sincronizar la libreta de direcciones">
<!ENTITY removeAccountFromAccountsOrCatsLabel "Eliminar la libreta de direcciones">

<!ENTITY IMPPMenuLabel "Conectar a">
