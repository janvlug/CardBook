<!ENTITY wdw_cardbookWindowLabel "CardBook">

<!ENTITY enableAccountsTooltip "启用或禁用通讯录搜索和同步">
<!ENTITY colorAccountsTooltip "显示通讯录搜索结果的颜色(当跨多个通讯录搜索时)">
<!ENTITY readonlyAccountsTooltip "设置通讯录为只读或读写模式">
<!ENTITY searchRemoteLabel "进行搜索后才会显示此通讯录的联系人">

<!ENTITY cardbookAccountMenuLabel "通讯录">

<!ENTITY cardbookAccountMenuAddServerLabel "新建通讯录">
<!ENTITY cardbookAccountMenuEditServerLabel "编辑通讯录">
<!ENTITY cardbookAccountMenuCloseServerLabel "删除通讯录">
<!ENTITY cardbookAccountMenuSyncLabel "同步通讯录">
<!ENTITY cardbookAccountMenuSyncsLabel "同步全部通讯录">

<!ENTITY cardbookContactsMenuLabel "联系人">

<!ENTITY cardbookToolsMenuLabel "工具">
<!ENTITY cardbookToolsMenuBirthdayListLabel "查看周年纪念日列表">
<!ENTITY cardbookToolsMenuSyncLightningLabel "添加周年纪念日到日历">
<!ENTITY cardbookToolsMenuFindAllDuplicatesLabel "在全部通讯录中查找重复联系人">
<!ENTITY cardbookToolsMenuFindSingleDuplicatesLabel "在当前通讯录中查找重复联系人">
<!ENTITY cardbookToolsMenuPrefsLabel "CardBook 偏好设置">

<!ENTITY cardbookToolbarLabel "CardBook 工具栏">
<!ENTITY cardbookToolbarAccesskey "a">
<!ENTITY cardbookABPaneToolbarLabel "CardBook 通讯录面板工具栏">
<!ENTITY cardbookABPaneToolbarAccesskey "d">
<!ENTITY CustomizeCardBookToolbarLabel "自定义…">
<!ENTITY CustomizeCardBookToolbarAccesskey "C">
<!ENTITY cardbookToolbarAddServerButtonLabel "新建通讯录">
<!ENTITY cardbookToolbarAddServerButtonTooltip "添加远程或本地通讯录">
<!ENTITY cardbookToolbarSyncButtonLabel "同步">
<!ENTITY cardbookToolbarSyncButtonTooltip "同步全部远程通讯录">
<!ENTITY cardbookToolbarWriteButtonLabel "编写">
<!ENTITY cardbookToolbarWriteButtonTooltip "新建消息">
<!ENTITY cardbookToolbarChatButtonLabel "连接到">
<!ENTITY cardbookToolbarChatButtonTooltip "发送即时消息或聊天">
<!ENTITY cardbookToolbarConfigurationButtonLabel "偏好设置">
<!ENTITY cardbookToolbarConfigurationButtonTooltip "CardBook 偏好设置">
<!ENTITY cardbookToolbarAddContactButtonLabel "新建联系人">
<!ENTITY cardbookToolbarAddContactButtonTooltip "新建通讯录联系人">
<!ENTITY cardbookToolbarAddListButtonLabel "新建列表">
<!ENTITY cardbookToolbarAddListButtonTooltip "新建邮件列表">
<!ENTITY cardbookToolbarEditButtonLabel "编辑">
<!ENTITY cardbookToolbarEditButtonTooltip "编辑已选联系人">
<!ENTITY cardbookToolbarRemoveButtonLabel "删除">
<!ENTITY cardbookToolbarRemoveButtonTooltip "删除已选联系人">
<!ENTITY cardbookToolbarPrintButtonLabel "打印预览">
<!ENTITY cardbookToolbarPrintButtonTooltip "已选的打印预览">
<!ENTITY cardbookToolbarBackButtonLabel "撤销">
<!ENTITY cardbookToolbarBackButtonTooltip "向后一个操作">
<!ENTITY cardbookToolbarForwardButtonLabel "重做">
<!ENTITY cardbookToolbarForwardButtonTooltip "向前一个操作">
<!ENTITY cardbookToolbarAppMenuButtonLabel "CardBook 菜单">
<!ENTITY cardbookToolbarAppMenuButtonTooltip "CardBook 菜单">
<!ENTITY cardbookToolbarSearchBoxLabel "联系人搜索结果">
<!ENTITY cardbookToolbarSearchBoxTooltip "联系人搜索结果">
<!ENTITY cardbookToolbarComplexSearchLabel "保存的搜索结果">
<!ENTITY cardbookToolbarComplexSearchTooltip "新建新保存的搜索文件夹">
<!ENTITY cardbookToolbarThMenuButtonLabel "应用菜单">
<!ENTITY cardbookToolbarThMenuButtonTooltip "显示缩写菜单">
<!ENTITY cardbookToolbarABMenuLabel "通讯录视图">
<!ENTITY cardbookToolbarABMenuTooltip "筛选通讯录显示方式">

<!ENTITY generalTabLabel "常规">
<!ENTITY mailPopularityTabLabel "邮件优先级">
<!ENTITY technicalTabLabel "技术的">
<!ENTITY vCardTabLabel "vCard">

<!ENTITY categoriesGroupboxLabel "类别">
<!ENTITY noteTabLabel "备注">

<!ENTITY miscGroupboxLabel "杂项">
<!ENTITY othersGroupboxLabel "其他">
<!ENTITY techGroupboxLabel "技术的">
<!ENTITY labelGroupboxLabel "标签">
<!ENTITY class1Label "类">
<!ENTITY geoLabel "地理位置">
<!ENTITY mailerLabel "邮件">
<!ENTITY agentLabel "代理">
<!ENTITY keyLabel "键">
<!ENTITY photolocalURILabel "本地照片">
<!ENTITY logolocalURILabel "本地徽标">
<!ENTITY soundlocalURILabel "本地声音">
<!ENTITY photoURILabel "照片">
<!ENTITY logoURILabel "徽标">
<!ENTITY soundURILabel "声音">
<!ENTITY prodidLabel "Prodid">
<!ENTITY sortstringLabel "排序字符串">
<!ENTITY uidLabel "联系人 ID">
<!ENTITY versionLabel "版本">
<!ENTITY tzLabel "时区">

<!ENTITY dirPrefIdLabel "通讯录 ID">
<!ENTITY cardurlLabel "Card URL">
<!ENTITY cacheuriLabel "缓存 URI">
<!ENTITY revLabel "最近更新">
<!ENTITY etagLabel "Etag">

<!ENTITY localizeadrTreeLabel "在地图显示">
<!ENTITY toEmailEmailTreeLabel "编写新消息">
<!ENTITY ccEmailEmailTreeLabel "编写新消息 (复制 / 抄送)">
<!ENTITY bccemailemailTreeLabel "编写新消息 (复制 / 密送)">
<!ENTITY findemailemailTreeLabel "查找关联此邮件地址的邮件">
<!ENTITY findeventemailTreeLabel "查找关联此邮件地址的日历事件">
<!ENTITY openURLTreeLabel "打开 URL">

<!ENTITY toEmailCardFromAccountsOrCatsLabel "编写新消息">
<!ENTITY toEmailCardFromCardsLabel "编写新消息">
<!ENTITY ccEmailCardFromAccountsOrCatsLabel "编写新消息 (复制 / 抄送)">
<!ENTITY ccEmailCardFromCardsLabel "编写新消息 (复制 / 抄送)">
<!ENTITY bccEmailCardFromAccountsOrCatsLabel "编写新消息 (复制 / 密送)">
<!ENTITY bccEmailCardFromCardsLabel "编写新消息 (复制 / 密送)">
<!ENTITY shareCardByEmailFromAccountsOrCatsLabel "以电子邮件分享">
<!ENTITY shareCardByEmailFromCardsLabel "以电子邮件分享">
<!ENTITY categoryLabel "类别">
<!ENTITY findEmailsFromCardsLabel "查找关联此联系人的邮件">
<!ENTITY findEventsFromCardsLabel "查找关联此联系人的日历事件">
<!ENTITY localizeCardFromCardsLabel "在地图显示">
<!ENTITY openURLCardFromCardsLabel "打开 URL">
<!ENTITY cutCardFromAccountsOrCatsLabel "剪切">
<!ENTITY cutCardFromCardsLabel "剪切">
<!ENTITY copyCardFromAccountsOrCatsLabel "复制">
<!ENTITY copyCardFromCardsLabel "复制">
<!ENTITY pasteCardFromAccountsOrCatsLabel "粘贴">
<!ENTITY pasteCardFromCardsLabel "粘贴">
<!ENTITY pasteEntryLabel "粘贴项">
<!ENTITY exportCardToFileLabel "导出到文件">
<!ENTITY exportCardToDirLabel "导出到目录">
<!ENTITY importCardFromFileLabel "从文件导入联系人">
<!ENTITY importCardFromDirLabel "从目录导入联系人">
<!ENTITY findDuplicatesFromAccountsOrCatsLabel "在当前通讯录中查找重复联系人">
<!ENTITY generateFnFromAccountsOrCatsLabel "生成显示名称">
<!ENTITY mergeCardsFromCardsLabel "合并联系人">
<!ENTITY duplicateCardFromCardsLabel "复制联系人">
<!ENTITY convertListToCategoryFromCardsLabel "转换列表为类别">
<!ENTITY editAccountFromAccountsOrCatsLabel "编辑通讯录">
<!ENTITY syncAccountFromAccountsOrCatsLabel "同步通讯录">
<!ENTITY removeAccountFromAccountsOrCatsLabel "删除通讯录">

<!ENTITY IMPPMenuLabel "连接到">
