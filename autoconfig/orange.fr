<clientConfig version="1.1">
	<carddavProvider id="CardDAVEngine">
		<carddavURL>https://carddav.orange.fr/addressbooks/%EMAILADDRESS%</carddavURL>
		<vCardVersion>3.0</vCardVersion>
	</carddavProvider>
</clientConfig>